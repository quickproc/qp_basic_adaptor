#!/bin/bash

echo "Stop hook, called when all batch processing is stopped."
echo "Home Dir:   ${1}"
echo "Source Dir: ${2}"
echo "User name:  ${3}"
echo "User UID:   ${4}"
echo "User GID:   ${5}"
